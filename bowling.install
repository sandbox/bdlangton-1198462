<?php

/**
 * @file
 *   Bowling module install/schema hooks.
 */

/**
 * Implements hook_schema().
 */
function bowling_schema() {
  $schema = array();

  // set up the 'bowling' table schema
  $schema['bowling'] = array(
    'description' => 'Table for storing bowling scores for the bowling module.',
    'fields' => array(
      'id' => array(
        'description' => 'Unique ID for this entry.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'uid' => array(
        'description' => 'The user ID of the user submitting the score.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'aid' => array(
        'description' => 'The alley ID of the alley the bowling was done at.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'type' => array(
        'description' => 'Type of bowling event (practice, league, 9-pin, etc).',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1,
      ),
      'date' => array(
        'description' => 'Date that the score was bowled.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'frame1a' => array(
        'description' => 'Frame 1a.',
        'type' => 'int',
      ),
      'frame1b' => array(
        'description' => 'Frame 1b.',
        'type' => 'int',
      ),
      'frame2a' => array(
        'description' => 'Frame 2a.',
        'type' => 'int',
      ),
      'frame2b' => array(
        'description' => 'Frame 2b.',
        'type' => 'int',
      ),
      'frame3a' => array(
        'description' => 'Frame 3a.',
        'type' => 'int',
      ),
      'frame3b' => array(
        'description' => 'Frame 3b.',
        'type' => 'int',
      ),
      'frame4a' => array(
        'description' => 'Frame 4a.',
        'type' => 'int',
      ),
      'frame4b' => array(
        'description' => 'Frame 4b.',
        'type' => 'int',
      ),
      'frame5a' => array(
        'description' => 'Frame 5a.',
        'type' => 'int',
      ),
      'frame5b' => array(
        'description' => 'Frame 5b.',
        'type' => 'int',
      ),
      'frame6a' => array(
        'description' => 'Frame 6a.',
        'type' => 'int',
      ),
      'frame6b' => array(
        'description' => 'Frame 6b.',
        'type' => 'int',
      ),
      'frame7a' => array(
        'description' => 'Frame 7a.',
        'type' => 'int',
      ),
      'frame7b' => array(
        'description' => 'Frame 7b.',
        'type' => 'int',
      ),
      'frame8a' => array(
        'description' => 'Frame 8a.',
        'type' => 'int',
      ),
      'frame8b' => array(
        'description' => 'Frame 8b.',
        'type' => 'int',
      ),
      'frame9a' => array(
        'description' => 'Frame 9a.',
        'type' => 'int',
      ),
      'frame9b' => array(
        'description' => 'Frame 9b.',
        'type' => 'int',
      ),
      'frame10a' => array(
        'description' => 'Frame 10a.',
        'type' => 'int',
      ),
      'frame10b' => array(
        'description' => 'Frame 10b.',
        'type' => 'int',
      ),
      'frame10c' => array(
        'description' => 'Frame 10c.',
        'type' => 'int',
      ),
      'total' => array(
        'description' => 'The total score.',
        'type' => 'int',
      ),
      'split1' => array(
        'description' => 'Tells if the user got a split on this frame.',
        'type' => 'int',
      ),
      'split2' => array(
        'description' => 'Tells if the user got a split on this frame.',
        'type' => 'int',
      ),
      'split3' => array(
        'description' => 'Tells if the user got a split on this frame.',
        'type' => 'int',
      ),
      'split4' => array(
        'description' => 'Tells if the user got a split on this frame.',
        'type' => 'int',
      ),
      'split5' => array(
        'description' => 'Tells if the user got a split on this frame.',
        'type' => 'int',
      ),
      'split6' => array(
        'description' => 'Tells if the user got a split on this frame.',
        'type' => 'int',
      ),
      'split7' => array(
        'description' => 'Tells if the user got a split on this frame.',
        'type' => 'int',
      ),
      'split8' => array(
        'description' => 'Tells if the user got a split on this frame.',
        'type' => 'int',
      ),
      'split9' => array(
        'description' => 'Tells if the user got a split on this frame.',
        'type' => 'int',
      ),
      'split10' => array(
        'description' => 'Tells if the user got a split on this frame.',
        'type' => 'int',
      ),
      'tenpin1' => array(
        'description' => 'Tells if the user picked up a 10 pin on this frame.',
        'type' => 'int',
      ),
      'tenpin2' => array(
        'description' => 'Tells if the user picked up a 10 pin on this frame.',
        'type' => 'int',
      ),
      'tenpin3' => array(
        'description' => 'Tells if the user picked up a 10 pin on this frame.',
        'type' => 'int',
      ),
      'tenpin4' => array(
        'description' => 'Tells if the user picked up a 10 pin on this frame.',
        'type' => 'int',
      ),
      'tenpin5' => array(
        'description' => 'Tells if the user picked up a 10 pin on this frame.',
        'type' => 'int',
      ),
      'tenpin6' => array(
        'description' => 'Tells if the user picked up a 10 pin on this frame.',
        'type' => 'int',
      ),
      'tenpin7' => array(
        'description' => 'Tells if the user picked up a 10 pin on this frame.',
        'type' => 'int',
      ),
      'tenpin8' => array(
        'description' => 'Tells if the user picked up a 10 pin on this frame.',
        'type' => 'int',
      ),
      'tenpin9' => array(
        'description' => 'Tells if the user picked up a 10 pin on this frame.',
        'type' => 'int',
      ),
      'tenpin10' => array(
        'description' => 'Tells if the user picked up a 10 pin on this frame.',
        'type' => 'int',
      ),
      'submitted' => array(
        'description' => 'Timestamp of when the score was submitted.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('id'),
  );
  
  // set up the 'bowling_types' table schema
  $schema['bowling_types'] = array(
    'description' => 'Bowling types such as league, practice, etc.',
    'fields' => array(
      'id' => array(
        'description' => 'Bowling type ID.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'type' => array(
        'description' => 'The user ID of the user submitting the score.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('id'),
  );

  return $schema;
}

/**
 * Implements hook_install().
 */
function bowling_install() {
  // add initial bowling types to the database
  $fields = array(
    'id'    => 1,
    'type' => 'League',
  );
  db_insert('bowling_types')
    ->fields($fields)
    ->execute();
    
  $fields = array(
    'id'    => 2,
    'type' => 'Tournament',
  );
  db_insert('bowling_types')
    ->fields($fields)
    ->execute();
    
  $fields = array(
    'id'    => 3,
    'type' => 'Practice',
  );
  db_insert('bowling_types')
    ->fields($fields)
    ->execute();
    
  $fields = array(
    'id'    => 4,
    'type' => '9 Pin No Tap',
  );
  db_insert('bowling_types')
    ->fields($fields)
    ->execute();
}

/**
 * Implements hook_uninstall().
 */
function bowling_uninstall() {
  // delete all variables set by the bowling module
  variable_del('bowling_tenpin');
  variable_del('bowling_splits');
  variable_del('bowling_only_scores');
}
