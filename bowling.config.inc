<?php

/**
 * @file
 * Configuration options for the bowling module.
 */

/**
 * Configuration options for the bowling module.
 */
function bowling_config() {
  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('Configure the bowling module.'),
  );
  $form['tenpin'] = array(
    '#type' => 'checkbox',
    '#title' => t('Keep track of ten pins.'),
    '#default_value' => variable_get('bowling_tenpin'),
    '#description' => "",
  );
  $form['splits'] = array(
    '#type' => 'checkbox',
    '#title' => t('Keep track of splits.'),
    '#default_value' => variable_get('bowling_splits'),
    '#description' => "",
  );
  $form['only_scores'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow users to enter only scores (not frame by frame information).'),
    '#default_value' => variable_get('bowling_only_scores'),
    '#description' => "",
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  
  return $form;
}

/**
 * Bowling configuration submit handler.
 * 
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function bowling_config_submit($form, &$form_state) {
  // set the form information into variables
  variable_set('bowling_tenpin', $form['tenpin']['#value']);
  variable_set('bowling_splits', $form['splits']['#value']);
  variable_set('bowling_only_scores', $form['only_scores']['#value']);
  
  drupal_set_message(t('Saved changes.'));
}
