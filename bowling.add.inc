<?php

/**
 * @file
 * Functions for adding bowling scores.
 */

/**
 * Add bowling scores form.
 */
function bowling_scores_add() {
  $form['frame1a'] = array(
    '#type' => 'textfield',
    '#size' => 5,
    '#maxlength' => 5,
  );
  $form['frame1b'] = array(
    '#type' => 'textfield',
    '#title' => t('Frame 1b'),
    '#size' => 5,
    '#maxlength' => 5,
  );
  $form['frame2a'] = array(
    '#type' => 'textfield',
    '#title' => t('Frame 2a'),
    '#size' => 5,
    '#maxlength' => 5,
  );
  $form['frame2b'] = array(
    '#type' => 'textfield',
    '#title' => t('Frame 2b'),
    '#size' => 5,
    '#maxlength' => 5,
  );
  $form['frame3a'] = array(
    '#type' => 'textfield',
    '#title' => t('Frame 3a'),
    '#size' => 5,
    '#maxlength' => 5,
  );
  $form['frame3b'] = array(
    '#type' => 'textfield',
    '#title' => t('Frame 3b'),
    '#size' => 5,
    '#maxlength' => 5,
  );
  $form['frame4a'] = array(
    '#type' => 'textfield',
    '#title' => t('Frame 4a'),
    '#size' => 5,
    '#maxlength' => 5,
  );
  $form['frame4b'] = array(
    '#type' => 'textfield',
    '#title' => t('Frame 4b'),
    '#size' => 5,
    '#maxlength' => 5,
  );
  $form['frame5a'] = array(
    '#type' => 'textfield',
    '#title' => t('Frame 5a'),
    '#size' => 5,
    '#maxlength' => 5,
  );
  $form['frame5b'] = array(
    '#type' => 'textfield',
    '#title' => t('Frame 5b'),
    '#size' => 5,
    '#maxlength' => 5,
  );
  $form['frame6a'] = array(
    '#type' => 'textfield',
    '#title' => t('Frame 6a'),
    '#size' => 5,
    '#maxlength' => 5,
  );
  $form['frame6b'] = array(
    '#type' => 'textfield',
    '#title' => t('Frame 6b'),
    '#size' => 5,
    '#maxlength' => 5,
  );
  $form['frame7a'] = array(
    '#type' => 'textfield',
    '#title' => t('Frame 7a'),
    '#size' => 5,
    '#maxlength' => 5,
  );
  $form['frame7b'] = array(
    '#type' => 'textfield',
    '#title' => t('Frame 7b'),
    '#size' => 5,
    '#maxlength' => 5,
  );
  $form['frame8a'] = array(
    '#type' => 'textfield',
    '#title' => t('Frame 8a'),
    '#size' => 5,
    '#maxlength' => 5,
  );
  $form['frame8b'] = array(
    '#type' => 'textfield',
    '#title' => t('Frame 8b'),
    '#size' => 5,
    '#maxlength' => 5,
  );
  $form['frame9a'] = array(
    '#type' => 'textfield',
    '#title' => t('Frame 9a'),
    '#size' => 5,
    '#maxlength' => 5,
  );
  $form['frame9b'] = array(
    '#type' => 'textfield',
    '#title' => t('Frame 9b'),
    '#size' => 5,
    '#maxlength' => 5,
  );
  $form['frame10a'] = array(
    '#type' => 'textfield',
    '#title' => t('Frame 10a'),
    '#size' => 5,
    '#maxlength' => 5,
  );
  $form['frame10b'] = array(
    '#type' => 'textfield',
    '#title' => t('Frame 10b'),
    '#size' => 5,
    '#maxlength' => 5,
  );
  $form['frame10c'] = array(
    '#type' => 'textfield',
    '#title' => t('Frame 10c'),
    '#size' => 5,
    '#maxlength' => 5,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  
  return $form;
}

/**
 * Bowling add score validation handler.
 * 
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function bowling_scores_add_validate($form, &$form_state) {
}

/**
 * Bowling add score submit handler.
 * 
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function bowling_scores_add_submit($form, &$form_state) {
  $return_value = NULL;
  try {
    $return_value = db_insert('bowling')
                      ->fields($form)
                      ->execute();
  }
  catch (Exception $e) {
    drupal_set_message(t('db_insert failed. Message = %message, query= %query',
      array('%message' => $e->getMessage(), '%query' => $e->query_string)), 'error');
  }
  drupal_set_message(t('Submitted scores.'));
  
  return $return_value;
}
